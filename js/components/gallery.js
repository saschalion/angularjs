define(['./module'], function (components) {
    'use strict';

    components.component('gallery', {
        templateUrl: '/partials/html/gallery.html',
        controller: 'GalleryCtrl'
    });
});
