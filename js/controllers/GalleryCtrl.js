define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('GalleryCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
        $scope.imagesBaseUrl = '/images/gallery/';
        var currentIndex = 0;

        $scope.slides = [
            {
                title: 'Дилемма как исчисление предикатов',
                subTitle: 'Мир, как принято считать, создает данный дедуктивный метод',
                image: '1.jpg'
            },
            {
                title: 'Дилемма как исчисление предикатов',
                subTitle: 'Мир, как принято считать, создает данный дедуктивный метод',
                image: '2.jpg'
            },
            {
                title: 'Дилемма как исчисление предикатов',
                subTitle: 'Мир, как принято считать, создает данный дедуктивный метод',
                image: '3.jpg'
            }
        ];

        $scope.onPrev = function() {
            if ( currentIndex > 0 ) {
                currentIndex--;
            }
        };

        $scope.onNext = function() {
            if ( currentIndex != this.slides.length - 1 ) {
                currentIndex++;
            }
        };

        $scope.currentIndex = function() {
            return currentIndex;
        };

        $scope.isActiveSlide = function(index) {
            return index == currentIndex;
        };

        $scope.setActiveSlide = function(index) {
            if ( index != currentIndex ) {
                currentIndex = index;
            }
        }
    }]);
});
