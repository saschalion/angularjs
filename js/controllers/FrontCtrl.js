define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('FrontCtrl', ['$scope', '$rootScope', '$timeout', function ($scope, $rootScope, $timeout) {
        $scope.loadingHtml = '/partials/html/loading.html';
        $scope.contentHtml = '/partials/html/content.html';
        $scope.isLoading = true;

        $timeout(function() {
            $scope.isLoading = false;
        }, 600);

        $scope.images = [
            '1.jpg',
            '2.jpg',
            '3.jpg',
            '4.jpg'
        ]
    }]);
});