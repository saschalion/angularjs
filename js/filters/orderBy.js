define(['./module'], function (filters) {
    'use strict';

    return filters.filter('orderObjectBy', [function () {
        return function(items, field, reverse) {
            var filtered = [];
            for(var i in items) {
                if ( !items.hasOwnProperty(i) ) {
                    continue;
                }

                filtered.push(items[i]);
            }

            filtered.sort(function (a, b) {
                if ( a[field] ) {
                    return (a[field].toLowerCase() > b[field].toLowerCase() ? 1 : -1);
                }
            });

            if ( reverse ) {
                filtered.reverse();
            }

            return filtered;
        };
    }]);
});