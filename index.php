<!doctype html>
<html lang="en" class="window">
<head>
    <meta charset="utf-8">
    <title>AngularJs Test App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/favicon.ico?ver=<?php echo time(); ?>" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico?ver=<?php echo time(); ?>" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&amp;subset=cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
</head>
<body class="page">

<div data-ng-view></div>

<script src="/lib/requirejs/require.js" data-main="js/main.js"></script>
<!--<script src="/lib/requirejs/require.js"></script>-->
<!--<script src="/js/main-built.js"></script>-->
</body>
</html>
