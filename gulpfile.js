var gulp = require('gulp');
var source = require('vinyl-source-stream');
var stylus = require('gulp-stylus');
var watch = require('gulp-watch');
var nib = require('nib');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

var dirPath = 'css';

gulp.task('css', function() {
    gulp.src([
            dirPath + '/imports.styl',
            dirPath + '/bpl-normalize.styl',
            dirPath + '/main.styl',
            dirPath + '/**/*.styl']
    )
        .pipe(concat('main.styl'))
        .pipe(stylus({
            use: [nib()],
            compress: true
        }))
        .pipe(autoprefixer())
        .pipe(gulp.dest(dirPath));
});

gulp.task('watch', function() {
    watch(dirPath + '/**/*.styl', function() {
        return gulp.src([
                dirPath + '/bpl-normalize.styl',
                dirPath + '/main.styl',
                dirPath + '/**/*.styl'
            ]
        )
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(concat('main.styl'))
            .pipe(stylus({
                use: [
                    nib()
                ]
            }))
            .pipe(autoprefixer({
                cascade: false
            }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(dirPath));
    });
});

gulp.task('default', ['build']);
gulp.task('build', ['css']);

gulp.task('default', ['watch']);
